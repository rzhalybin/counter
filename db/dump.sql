-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.12-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win64
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица testcounter.counts
DROP TABLE IF EXISTS `counts`;
CREATE TABLE IF NOT EXISTS `counts` (
  `user_id` int(10) unsigned NOT NULL,
  `event_id` tinyint(3) unsigned NOT NULL,
  `date` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`event_id`,`date`),
  KEY `FK_counts_events` (`event_id`),
  CONSTRAINT `FK_counts_events` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testcounter.counts: ~11 rows (приблизительно)
DELETE FROM `counts`;
/*!40000 ALTER TABLE `counts` DISABLE KEYS */;
INSERT INTO `counts` (`user_id`, `event_id`, `date`) VALUES
	(2, 2, 1407110300),
	(2, 2, 1407440773),
	(2, 2, 1407445070),
	(2, 2, 1417604490),
	(2, 2, 1417604492),
	(1, 3, 1407488460),
	(1, 3, 1407488463),
	(1, 3, 1407489428),
	(1, 3, 1417604091),
	(1, 3, 1417604490),
	(1, 3, 1417604491),
	(2, 3, 1407440798),
	(2, 3, 1407445070);
/*!40000 ALTER TABLE `counts` ENABLE KEYS */;


-- Дамп структуры для таблица testcounter.events
DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testcounter.events: ~2 rows (приблизительно)
DELETE FROM `events`;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` (`id`, `name`) VALUES
	(1, 'add'),
	(2, 'view'),
	(3, 'edit');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;


-- Дамп структуры для таблица testcounter.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(256) NOT NULL,
  `token` varchar(30) NOT NULL,
  `event_id` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `token` (`token`),
  KEY `FK_users_events` (`event_id`),
  CONSTRAINT `FK_users_events` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testcounter.users: ~3 rows (приблизительно)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `password`, `email`, `token`, `event_id`) VALUES
	(1, 'admin', 'temjCCsjBECmU', 'mail1@mail.com', 'token1', 1),
	(2, 'user2', 'temjCCsjBECmU', 'mail2@mail.com', 'token2', 1),
	(3, 'user3', 'temjCCsjBECmU', 'mail3@mail.com', 'token3', 2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
