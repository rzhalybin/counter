<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
$series = array();
if ($data) {
	foreach ($data as $name => $values) {
		$series[] =  array(
			'name' => $name,
			'data' => $values
		);
	}
}
$this->widget(
    'booster.widgets.TbHighCharts',
    array(
        'options' => array(
			'chart' => array(
                'zoomType' => 'x',
				'type' => 'column'
            ),
            'title' => array(
                'text' => 'Counter',
            ),
            'subtitle' => array(
                'text' => 'Click and drag in the plot area to zoom in',
            ),
            'xAxis' => array(
                'type' => 'datetime',
                'minRange' => 3600  * 1000
            ),
            'yAxis' => array(
                'title' => array(
                    'text' =>  'amount',
                ),
                'minRange' => 1
            ),
            'legend' => array(
                'enabled' => false,
            ),
			'plotOptions' => array(
				'series' => array(
					'pointStart' => strtotime('-1 month') * 1000,
					'pointInterval' => 24 * 3600 * 1000 // one day
				)
			),
            'series' => $series
//            'series' => array(
//				array(
//					'name' => 'qqq',
//					'data' => array(
//							0.8446, 0.8445, 0.8444, 0.8451,    0.8418, 0.8264,    0.8258, 0.8232,    0.8233, 0.8258,
//					)
//				),
//				array(
//					'name' => 'www',
//					'data' => array(
//							0.9446, 0.9445, 0.9444, 0.9451,    0.9418, 0.9264,    0.9258, 0.9232,    0.9233, 0.9258,
//					)
//				)
//			)
        ),
        'htmlOptions' => array(
            'style' => 'min-width: 310px; height: 400px; margin: 0 auto'
        )
    )
);
echo '<br><br>';
$this->widget(
    'booster.widgets.TbPanel',
    array(
        'title' => 'Code snippet',
    	'context' => 'primary',
        'headerIcon' => 'home',
        'content' => $code
    )
);