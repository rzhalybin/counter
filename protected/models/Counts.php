<?php

/**
 * This is the model class for table "counts".
 *
 * The followings are the available columns in table 'counts':
 * @property string $user_id
 * @property integer $event_id
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Events $event
 */
class Counts extends CActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'counts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, event_id, date', 'required'),
			array('event_id', 'numerical', 'integerOnly' => true),
			array('user_id, date', 'length', 'max' => 10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, event_id, date', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'event' => array(self::BELONGS_TO, 'Events', 'event_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'user_id' => 'User',
			'event_id' => 'Event',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('user_id', $this->user_id, true);
		$criteria->compare('event_id', $this->event_id);
		$criteria->compare('date', $this->date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Counts the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function chart_data() 
	{
		$criteria = new CDbCriteria;
		$criteria->condition = '`date` > '.  strtotime('-1 month');
		
		$rows = $this->with('event')->findAll($criteria);
		$return = array();
		if ($rows) {
			foreach ($rows as $row) {
				$return[$row['event']['name']][] = array($row['date']*1000, 1);
			}
		}
		return $return;
	}
	
	public function get_data_by_range($data = array()) 
	{
		$condition = array();
		if (isset($data['user_id'])) {
			$condition[] = 'user_id = '.$data['user_id'];
		}
		if (isset($data['event_id'])) {
			$condition[] = 'event_id = '.$data['event_id'];
		}
		if (isset($data['start'])) {
			$condition[] = '`date` >= '.$data['start'];
		}
		if (isset($data['end'])) {
			$condition[] = '`date` < '.$data['end'];
		}
		$criteria = new CDbCriteria();
		if (!empty($condition)) {
			$criteria->condition = implode(' AND ', $condition);
		}
		return $this->findAll($criteria);
	}

}
