<?php

class CounterController extends Controller
{
	public function actionCount($token, $event = '')
	{
		$user = Users::model()->findByAttributes(array('token' => $token));
		if ($user) {
			if (!$event) {
				$event = $user['event_id'];
			}
			$event_row = Events::model()->findByAttributes(array('name' => $event));
			if (!$event_row) {
				$event_row = new Events();
				$event_row->setAttribute('name', $event);
				$event_row->save();
			}
//			$row = Counts::model()->findByAttributes(array('user_id' => $user['id'], 'event' => $event));
//			if (!$row) {
				$row = new Counts();
				$row->setAttribute('user_id', $user['id']);
				$row->setAttribute('event_id', $event_row['id']);
				$row->setAttribute('date', time());
				$row->save();
//			}
		}
	}

}