<?php

class StatsController extends Controller
{
	private $format = 'json';
	private $user_id;
	private $allow_formats = array('json', 'xml', 'csv');
	
	protected function beforeAction($action) {
		$token = Yii::app()->getRequest()->getQuery('token');
		$criteria = new CDbCriteria();
		$criteria->select = 'id';
		$criteria->condition = 'sha1(email) = :token';
		$criteria->params = array(':token' => $token);
		
		$user = Users::model()->find($criteria);
		if ($user) {
			$this->user_id = $user['id'];
		}
		
		$format = Yii::app()->getRequest()->getQuery('format');
		if ($format && in_array($format, $this->allow_formats)) {
			$this->format = $format;
		}
		return parent::beforeAction($action);
	}
	
	public function actionHour($event)
	{
		$hours = array();
		$day = 86400;
		$period = 3600;
		$event_row = Events::model()->findByAttributes(array('name' => $event));
		if ($this->user_id && $event_row) {
			$start = mktime(0,0,0);
			$end = $start + $day;
			
			$rows = Counts::model()->get_data_by_range(array(
				'user_id'  => $this->user_id,
				'event_id' => $event_row['id'],
				'start'    => $start,
				'end'      => $end,
			));
			if ($rows) {
				foreach ($rows as $row) {
					$step = $start + ((int)(($row['date']-$start)/$period)*$period);
					if (isset($hours[$step])) {
						++$hours[$step]['counter'];
					}
					else {
						$hours[$step] = array(
							'timestamp' => $step,
							'counter'   => 1,
						);
					}
				}
			}
		}
		echo $this->_send(array_values($hours));
	}
	
	public function actionDay($event)
	{
		$hours = array();
		$day = 86400;
		$event_row = Events::model()->findByAttributes(array('name' => $event));
		if ($this->user_id && $event_row) {
			$start = mktime(0,0,0);
			$end = $start + $day;
			$week_day = date('N');
			$week_start = $end - ($week_day*$day);
			
			$rows = Counts::model()->get_data_by_range(array(
				'user_id'  => $this->user_id,
				'event_id' => $event_row['id'],
				'start'    => $week_start,
				'end'      => $end,
			));
			if ($rows) {
				foreach ($rows as $row) {
					$step = $week_start + ((int)(($row['date']-$week_start)/$day)*$day);
					if (isset($hours[$step])) {
						++$hours[$step]['counter'];
					}
					else {
						$hours[$step] = array(
							'timestamp' => $step,
							'counter'   => 1,
						);
					}
				}
			}
		}
		echo $this->_send(array_values($hours));
	}
	
	public function actionWeek($event)
	{
		$hours = array();
		$day = 86400;
		$event_row = Events::model()->findByAttributes(array('name' => $event));
		if ($this->user_id && $event_row) {
			$start = mktime(0,0,0);
			$end = $start + $day;
			$month_day = date('j');
			$month_start = $end - ($month_day*$day);
			
			$rows = Counts::model()->get_data_by_range(array(
				'user_id'  => $this->user_id,
				'event_id' => $event_row['id'],
				'start'    => $month_start,
				'end'      => $end,
			));
			if ($rows) {
				$month_weeks = array();
				$start_week = $end_week = 0;
				for ($i = 0; $i < date('t'); $i++) {
					$time = $month_start+$i*$day;
					$week_day = date('N', $time);
					if ($week_day == 1) {
						$start_week = $time;
					}
					if ($week_day == 7) {
						if (!$start_week) {
							$start_week = $month_start;
						}
						$end_week = $time;
					}
					if ($end_week) {
						$month_weeks[$start_week] = $end_week + $day;
						$end_week = $start_week = 0;
					}
				}
				foreach ($rows as $row) {
					foreach ($month_weeks as $start_week => $end_week) {
						if ($row['date'] >= $start_week && $row['date'] < $end_week) {
							if (isset($hours[$start_week])) {
								++$hours[$start_week]['counter'];
							}
							else {
								$hours[$start_week] = array(
									'timestamp' => $start_week,
									'counter'   => 1,
								);
							}
						}
					}
				}
			}
		}
		echo $this->_send(array_values($hours));
	}

	private function _send($data) 
	{
		$return = '';
		$method = 'convert_to_'.$this->format;
		if (method_exists($this, $method)) {
			$return = $this->$method($data);
		}
		return $return;
	}
	
	private function convert_to_json($data)
	{
		return json_encode($data);
	}
	
	private function convert_to_xml($data)
	{
		$xmldata = '<?xml version="1.0" encoding="utf-8"?>'; 
        foreach ($data as $row) {
			$xmldata .= '<row>';
			foreach ($row as $k => $v) {
				$xmldata .= '<'.$k.'>'.$v.'</'.$k.'>';
			}
			$xmldata .= '</row>';
        }   
		return $xmldata;
	}
	
	private function convert_to_csv($data)
	{
		$return = array();
		if ($data) {
			$first = current($data);
			$return[] = implode(',', array_keys($first));
		}
		foreach ($data as $row) {
			$return[] = implode(',', $row);
		}
		return implode("\n", $return);
	}
	
}