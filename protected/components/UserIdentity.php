<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

	private $_id;
	private $_token;

	public function authenticate() {
		$record = Users::model()->findByAttributes(array('name' => $this->name));
		if ($record === null) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		} 
//		else if ($record->password !== crypt($this->password, $record->password)) {
//			$this->errorCode = self::ERROR_PASSWORD_INVALID;
//		} 
		else {
			$this->_id = $record->id;
			$this->_token = $record->token;
			$this->errorCode = self::ERROR_NONE;
		}
		return !$this->errorCode;
	}

	public function getId() {
		return $this->_id;
	}

	public function getToken() {
		return $this->_token;
	}

}
