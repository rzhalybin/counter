<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class CustomUser extends CWebUser {

	private $_token;

	public function init(){
       parent::init();
       if(!$this->getIsGuest()){
            $user = Users::model()->findByPk($this->getId());
			$this->_token = $user['token'];
       }
    }

	public function getToken() {
		return $this->_token;
	}

}
